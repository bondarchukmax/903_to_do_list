const taskForm = document.getElementById('taskForm');
const taskInput = document.getElementById('inputTask');
const taskList = document.getElementById('taskList');
let taskArray = [
    // { id: 0, value: "Task 1", checked: false },
    // { id: 1, value: "Task 2", checked: true  },
    // { id: 2, value: "Task 3", checked: false },
];
const localStorageArray = localStorage.getItem('taskArray');
if (localStorageArray){
    taskArray = JSON.parse(localStorageArray);
    for (let i = 0; i < taskArray.length; i++) {
        addToList(taskArray[i].id, taskArray[i].value, taskArray[i].checked);
    }
}

taskForm.onsubmit = function (event) {
    event.preventDefault();
    let id = taskArray.length ? taskArray[taskArray.length - 1].id : 0;
    id++;
    addTaskToArray(id, taskInput.value, false);
    addToList(id, taskInput.value, false);
};

function taskStatus(li) {
    li.onclick = function(event){
        if ((event.target.className === 'task-delete')) {
            const parentLi = event.target.parentElement;
            parentLi.remove();
            itarTaskArray( parentLi.id, function (i) {
                taskArray.splice(i,1);
            });
        } else  if (event.target.className === 'task-edit') {
            const input = li.children[0];
            const oldInputValue = input.value;
            const id = li.id;
            input.onblur = function(event) {
                if (input.className !== 'input-edit') {
                    return;
                }

              const conf = confirm('Сохранить изменения?');
              if (conf) {
                  for (let i = 0; i < taskArray.length; i++) {
                      if (taskArray[i].id.toString() === id) {
                          taskArray[i].value = input.value;
                          break
                      }
                  }
                  addTaskArrayToStorage()
              } else {
                  input.value = oldInputValue;
              }
              input.setAttribute('readonly', '');
              input.classList.remove('input-edit');
            };
            input.removeAttribute('readonly');
            input.classList.add('input-edit');
            input.focus();
        }
        else if (event.target.className === 'input-edit'){
            return;
        }
        else {
            li.classList.toggle('checked');
            // itarTaskArray( li.id, function (i) {
            //     taskArray[i].checked = !taskArray[i].checked;
            // });

            for (let i = 0; i < taskArray.length; i++) {
                if (taskArray[i].id.toString() === li.id) {
                    taskArray[i].checked = !taskArray[i].checked;
                    break;
                }
            }
        }

        addTaskArrayToStorage();
    }
}

function addToList(id, value, checked) {
    const li = document.createElement('li');
    const input = document.createElement('input');
    const buttonDelete = document.createElement('button');
    const buttonEdit = document.createElement('button');
    buttonDelete.classList.add('task-delete');
    buttonEdit.classList.add('task-edit');
    input.value = value;
    input.setAttribute('readonly', '' );
    li.append(input);
    li.append(buttonEdit);
    li.append(buttonDelete);
    li.id = id;
    if (checked){
        li.classList.add('checked')
    }
    taskList.append(li);
    taskStatus(li);
    taskForm.reset();
}

function addTaskToArray(id, value, checked) {
    taskArray.push({
        id: id,
        value: value,
        checked: checked
    });
    addTaskArrayToStorage();
}

function itarTaskArray(id, main ) {
    for (let i = 0; i < taskArray.length; i++) {
        if (taskArray[i].id.toString() === id) {
            main(i);
            break;
        }
    }
}

function addTaskArrayToStorage() {
    localStorage.setItem('taskArray', JSON.stringify(taskArray));
}